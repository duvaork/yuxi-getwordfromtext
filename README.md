# Introduction

You have to implement a method which will be returning an array with the words from the text.

# Problem Statement

#### Summary
The method to implement is `GetWordFromText`. It should return an array with the selected words of an input text. Please don't make use of the functions splice and slice.

#### Parameters
words is a string containing all the words separated by space (`' '`).
start is a number which indicates the initial word of the selection. Words are counted from index 1.
end is a number indicating the number of words to be choosen. This parameter is optional, if it is not defined the remaining words have to be returned.

#### Examples
```javascript
GetWordFromText("one two three", 2);
``` 
should return `["two", "three"]`

--------


```javascript
GetWordFromText("one two three", 2, 1);
```
should return `["two"]`

----------

```javascript
GetWordFromText("one two three", 2, 0);
```

should return `[]`

#### Be aware of

1. When input parameter words is null the method should throw "Not Null Allowed".
2. when input parameter start is less than one or bigger than the words number the method should throw "Argument Range Exception".
3. When input parameter end is bigger than the number of words after the start parameter the method should throw "Argument Range Exception".

The method should ignore all spaces at the beginning and at the end of the input text.