/*
  <summary>
  The method to implement is `GetWordFromText`. It should return an array with the selected words of an input text.
  </summary>
  <param name="words">String containing all the words separated by space.</param>
  <param name=start">Number which indicates the initial word of the selection. Words are counted from index 1.</param>
  <param name=end">End is number indicating the number of words to be choosen. This parameter is optional, if it is not defined the remaining words have to be returned.</param>
  <returns>
  Array of specific words from a given text.
  Example 1: GetWordFromText("one two three", 2) should return ["two", "three"]
  Example 2: GetWordFromText("one two three", 2, 1) should return ["two"]
  Example 3: GetWordFromText("one two three", 2, 1) should return "[]"
  Be aware of
  1. When input parameter words is null the method should throw "Not Null Allowed".
  2. when input parameter start is less than one or bigger than the words number the method should throw "Argument Range Exception".
  3. When input parameter end is bigger than the number of words after the start parameter the method should throw "Argument Range Exception".
  </returns>
*/
export const GetWordFromText = (words, start, end) =>
{

    // TODO: Implement logic HERE

}