import { GetWordFromText } from '../src/app'

describe('GetWordFromText', function () {

  it('should return empty if end is 0', function() {
    expect(GetWordFromText("One Two Three", 2, 0)).toEqual([]);
  });

  it('should return the selected word', function() {
    expect(GetWordFromText("One Two Three", 2, 1)).toEqual(["Two"]);
  });

  it('should return the selected words', function() {
    expect(GetWordFromText("One Two Three Four", 2, 2)).toEqual(["Two",  "Three"]);
  });

  it('should return the remaining words when end in undefined', function() {
    expect(GetWordFromText("One Two Three", 2)).toEqual(["Two",  "Three"]);
  });

  it('should throw "Not Null Allowed" when input parameter words is null.', function() {
    expect(() => GetWordFromText(null, 2)).toThrow("Not Null Allowed");
  });

  it('should throw "Argument Range Exception" when input parameter start is less than one.', function() {
    expect(() => GetWordFromText("One Two Three", 0)).toThrow("Argument Range Exception");
  });

  it('should throw "Argument Range Exception" when input parameter start is bigger than the words number.', function() {
    expect(() => GetWordFromText("One Two Three", 4)).toThrow("Argument Range Exception");
  });

  it('should throw "Argument Range Exception" when input parameter end is bigger than the number of words after the start parameter', function() {
    expect(() => GetWordFromText("One Two Three", 2, 3)).toThrow("Argument Range Exception");
  });

  it('should igone spaces and the end and the start of the input string', function() {
    expect(GetWordFromText("   One Two Three   ", 2, 1)).toEqual(["Two"]);
  });

});
